pipeline {
  agent {
    docker {
      image 'node'
    }
  }
  environment {
    QCLOUD_REGISTERY_USERNAME = credentials('QCLOUD_REGISTERY_USERNAME')
    QCLOUD_REGISTERY_PASSWORD = credentials('QCLOUD_REGISTERY_PASSWORD')
    QCLOUD_REGISTERY_ADDRESS = credentials('QCLOUD_REGISTERY_ADDRESS')
    QCLOUD_REGISTERY_NAMESPACE = credentials('QCLOUD_REGISTERY_NAMESPACE')
  }
  stages {
    stage('Preparation') {
      steps {
        sh 'node -v'
        sh 'npm -v'
      }
    }
    stage('Config') {
      steps {
        sh 'npm set registry https://registry.npm.taobao.org'
        sh 'npm set disturl https://npm.taobao.org/dist'
        sh 'npm set chromedriver_cdnurl http://cdn.npm.taobao.org/dist/chromedriver'
        sh 'npm set operadriver_cdnurl http://cdn.npm.taobao.org/dist/operadriver'
        sh 'npm set phantomjs_cdnurl http://cdn.npm.taobao.org/dist/phantomjs'
        sh 'npm set fse_binary_host_mirror https://npm.taobao.org/mirrors/fsevents'
        sh 'npm set sass_binary_site http://cdn.npm.taobao.org/dist/node-sass'
        sh 'npm set electron_mirror http://cdn.npm.taobao.org/dist/electron/'
      }
    }
    stage('Dependencies') {
      steps {
        sh 'npm cache verify'
        sh 'npm install'
      }
    }
    stage('Unit Test') {
      steps {
        sh 'npm run unit'
      }
    }
    stage('Build') {
      steps {
        sh 'npm run build'
      }
    }
    stage('Artifacts') {
      steps {
        sh 'tar -czf ${JOB_NAME}.tar.gz ./dist'
        archiveArtifacts artifacts: '${JOB_NAME}.tar.gz', fingerprint: true
      }
    }
    stage('Docker Image') {
      agent {
        docker {
          image 'docker:dind'
        }
      }
      steps {
        script {
          if (env.Branch == 'origin/master' || env.Branch == 'master') {
            env.TagName = 'latest'
          } else {
            env.TagName = env.Branch
          }
        }
        sh 'docker -v'
        sh 'docker build --no-cache -t ${QCLOUD_REGISTERY_ADDRESS}/${QCLOUD_REGISTERY_NAMESPACE}/${JOB_NAME}:${TagName} .'
        sh 'docker login ${QCLOUD_REGISTERY_ADDRESS} -u ${QCLOUD_REGISTERY_USERNAME} -p ${QCLOUD_REGISTERY_PASSWORD}'
        sh 'docker push ${QCLOUD_REGISTERY_ADDRESS}/${QCLOUD_REGISTERY_NAMESPACE}/${JOB_NAME}:${TagName}'
        sh 'docker logout ${QCLOUD_REGISTERY_ADDRESS}'
      }
    }
  }
}
