
FROM nginx:stable-alpine
LABEL Name=ci-test Version=0.0.1
COPY ./dist /www/html
COPY ./nginx.conf /etc/nginx/nginx.conf
EXPOSE 80
